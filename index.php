<?php

include 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;

$request = new Request(
    $_GET,
    $_POST,
    [],
    $_COOKIE,
    $_FILES,
    $_SERVER
);


$loader = new \Twig\Loader\FilesystemLoader('./templates');
$twig = new \Twig\Environment($loader,['debug' => true]);
$twig->addExtension(new \Twig\Extension\DebugExtension());


$pages = [];
$details = [];
$error = false;

if ($request->query->has('url') && '' !== $request->query->get('url')) {
    $parser = new \Smalot\PdfParser\Parser();

    try {
        $pdf = $parser->parseContent(file_get_contents($request->query->get('url')));
        $details  = $pdf->getDetails();
        $pages = $pdf->getPages();
    } catch (\Exception $exception) {
        $error = $exception->getMessage();
    }
}

$template = $twig->load('index.html.twig');
echo $template->render(['pages' => $pages, 'details' => $details, 'error' => $error]);

/*
// Parse pdf file and build necessary objects.


// Retrieve all pages from the pdf file.


// Loop over each page to extract text.
*/